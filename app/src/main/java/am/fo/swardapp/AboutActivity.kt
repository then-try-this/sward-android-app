/*
   Sward App Copyright (C) 2020 FoAM Kernow

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package am.fo.swardapp

import am.fo.swardapp.databinding.ActivityAboutBinding
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod

class AboutActivity : SwardActivity() {

    private lateinit var binding: ActivityAboutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAboutBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.inflateMenu(R.menu.menu_main);
        setSupportActionBar(binding.toolbar)

        binding.about1.text = Html.fromHtml(getString(R.string.about_text),Html.FROM_HTML_MODE_LEGACY)
        binding.about2.text = Html.fromHtml(getString(R.string.about_2),Html.FROM_HTML_MODE_LEGACY)
        binding.about3.text = Html.fromHtml(getString(R.string.about_3),Html.FROM_HTML_MODE_LEGACY)

        binding.about1.movementMethod = LinkMovementMethod.getInstance()
        binding.about2.movementMethod = LinkMovementMethod.getInstance()
        binding.about3.movementMethod = LinkMovementMethod.getInstance()

    }

}
