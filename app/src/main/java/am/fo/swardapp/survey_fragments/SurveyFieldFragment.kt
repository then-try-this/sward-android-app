/*
   Sward App Copyright (C) 2020 FoAM Kernow

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package am.fo.swardapp.survey_fragments

import am.fo.swardapp.FieldListAdapter
import am.fo.swardapp.R
import am.fo.swardapp.SwardActivity
import am.fo.swardapp.SwardFragment
import am.fo.swardapp.databinding.FragmentSurveyFieldBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

class SurveyFieldFragment : SwardFragment() {
    private lateinit var binding: FragmentSurveyFieldBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSurveyFieldBinding.inflate(layoutInflater)

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = FieldListAdapter(requireContext(),this)
        binding.fieldsRecyclerView.adapter = adapter
        binding.fieldsRecyclerView.layoutManager = LinearLayoutManager(requireContext())

        val sa: SwardActivity = activity as SwardActivity
        sa.swardViewModel.allFields.observe(viewLifecycleOwner) { fields ->
            // Update the cached copy of the words in the adapter.
            fields?.let { adapter.setFields(it) }
        }

    }
}