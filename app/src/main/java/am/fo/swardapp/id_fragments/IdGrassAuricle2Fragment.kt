/*
   Sward App Copyright (C) 2020 FoAM Kernow

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package am.fo.swardapp.id_fragments

import am.fo.swardapp.R
import am.fo.swardapp.databinding.FragmentIdGrassAuricle2Binding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

class IdGrassAuricle2Fragment : Fragment() {
    private lateinit var binding: FragmentIdGrassAuricle2Binding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentIdGrassAuricle2Binding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.one.setOnClickListener {
            findNavController().navigate(R.id.action_idGrassAuricle2Fragment_to_idResultFragment)
        }
        binding.two.setOnClickListener {
            findNavController().navigate(R.id.action_idGrassAuricle2Fragment_to_idResultFragment2)
        }
        binding.three.setOnClickListener {
            findNavController().navigate(R.id.action_idGrassAuricle2Fragment_to_idResultFragment3)
        }
    }
}