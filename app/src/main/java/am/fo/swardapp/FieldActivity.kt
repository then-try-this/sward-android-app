/*
   Sward App Copyright (C) 2020 FoAM Kernow

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package am.fo.swardapp

import am.fo.swardapp.data.DateWrangler
import am.fo.swardapp.data.Field
import am.fo.swardapp.databinding.ActivityFieldBinding
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import java.util.*

class FieldActivity : SwardActivity() {

    private var fieldId: Long = 0
    private lateinit var binding: ActivityFieldBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFieldBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.inflateMenu(R.menu.menu_main);

        setSupportActionBar(binding.toolbar)

        fieldId = intent.getLongExtra("FIELD_ID", 0)

        // set up the widgets
        swardViewModel.getBiodiversity(fieldId, 15).observe(this, { surveys ->
            swardViewModel.getSown(fieldId).observe(this, {
                binding.fieldCanvasView.addData(surveys,it.size)
            })
        })

        val cal = Calendar.getInstance()

        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                binding.editFieldDate.text = DateWrangler.timeAsView(cal.time)
            }

        binding.editFieldDate.setOnClickListener {
            DatePickerDialog(
                this, dateSetListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        ArrayAdapter.createFromResource(
            this,
            R.array.soil_types,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            binding.editFieldSoilType.adapter = adapter
        }

        swardViewModel.getField(fieldId).observe(this, { field ->
            field?.let {
                binding.toolbar.title = field.name
                binding.editFieldName.setText(field.name)
                binding.editFieldDate.text = DateWrangler.dateInternalToView(field.dateSown)
                binding.editFieldSoilType.setSelection(field.soilType)
                binding.editFieldNotes.setText(field.notes)
            }
        })

        binding.fieldSurvey.setOnClickListener {
            Intent(this, SurveyActivity::class.java).let {
                it.putExtra("FIELD_ID", fieldId)
                it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    this.finishAffinity()
                }
                this.startActivity(it)
            }
        }

        binding.fieldResults.setOnClickListener {
            Intent(this, FieldDetailActivity::class.java).let {
                it.putExtra("FIELD_ID", fieldId)
                this.startActivity(it)
            }
        }

        binding.fieldDelete.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.field_delete))
            builder.setMessage(getString(R.string.field_delete_body))
            builder.setPositiveButton(getString(R.string.field_delete_yes)) { dialog, which ->
                swardViewModel.deleteField(fieldId)
                val intent = Intent(this, FarmActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            builder.setNegativeButton(getString(R.string.field_delete_no)) { dialog, which ->
                // don't need to to anything...
            }
            builder.show()
        }


    }

    // all exits go through the gift shop, it seems
    override fun onPause() {
        super.onPause()
        val field = Field(
            binding.editFieldName.text.toString(),
            DateWrangler.dateViewToInternal(binding.editFieldDate.text.toString()),
            binding.editFieldSoilType.selectedItemPosition,
            binding.editFieldNotes.text.toString()
        )
        field.fieldId = fieldId
        swardViewModel.updateField(field)
    }

}