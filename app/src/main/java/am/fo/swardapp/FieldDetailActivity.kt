/*
   Sward App Copyright (C) 2020 FoAM Kernow

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package am.fo.swardapp

import am.fo.swardapp.databinding.ActivityFieldBinding
import am.fo.swardapp.databinding.ActivityFieldDetailBinding
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager

class FieldDetailActivity : SwardActivity() {
    private lateinit var binding: ActivityFieldDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFieldDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.toolbar.inflateMenu(R.menu.menu_main);

        setSupportActionBar(binding.toolbar)

        val fieldId = intent.getLongExtra("FIELD_ID",0)

        val adapter = SpeciesDetailAdapter(this)
        binding.speciesRecyclerView.adapter = adapter
        binding.speciesRecyclerView.layoutManager = LinearLayoutManager(this)

        swardViewModel.getSpeciesAndSurveyCounts(fieldId).observe(this, { species ->
            species?.let { adapter.setFields(it) }
        })
    }
}